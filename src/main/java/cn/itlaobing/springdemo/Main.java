package cn.itlaobing.springdemo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
	
	static Log log=LogFactory.getLog(Main.class);

	public static void main(String[] args) {
		//加载Spring配置文件
		
		/**
		 * ApplicationContext 是spring提供的一个接口表示 spring容器
		 * 
		 * 因为我们定义的是一个xml配置文件，并且放在了classpath中,所以加载这个配置文件需要使用ClassPathXmlApplicationContext 实现类
		 */
		ApplicationContext ctx=new ClassPathXmlApplicationContext("applicationContext.xml");
		
		//从容器中获取对象  通过名称和Class类型来获取到这个对象
		UserService userService= ctx.getBean("userService",UserService.class);
		
		log.info(userService.sayHello("Spring!"));
		

	}

}